For centminmod.com based LEMP installs on CentOS 7

    yum -y -q install autoconf automake bind-utils gcc libtool make nmap-netcat ntp pam-devel unzip wget pam pam-devel
    cd /svr-setup/
    git clone https://github.com/google/google-authenticator-libpam
    cd google-authenticator-libpam/
    ./bootstrap.sh
    ./configure --prefix=/usr
    make dist
    cp google-authenticator-1.03.tar.gz ~/rpmbuild/SOURCES/

A few mods to rpm.spec for

    * Mon Feb 06 2017 George Liu <centminmod.com> - 1.03
    - custom 1.0.3 build for centminmod.com environments

Then build

    rpmbuild -ba contrib/rpm.spec

Built RPMs will be located at:

* /root/rpmbuild/SRPMS/google-authenticator-1.03-1.el7.centos.src.rpm
* /root/rpmbuild/RPMS/x86_64/google-authenticator-1.03-1.el7.centos.x86_64.rpm
* /root/rpmbuild/RPMS/x86_64/google-authenticator-debuginfo-1.03-1.el7.centos.x86_64.rpm

Change log

    rpm -qa --changelog google-authenticator
    * Mon Feb 06 2017 George Liu <centminmod.com> - 1.03
    - custom 1.03 build for centminmod.com environments
    
    * Wed Jan 13 2016 Dan Molik <dan@d3fy.net> - 1.01
    - A new and updated build for google-authenticator


Post Install Configuration SSH commands

    ls -lah /lib64/security
    cd /usr/lib64/security/
    ln -s /lib64/security/pam_google_authenticator.so pam_google_authenticator.so
    ln -s /lib64/security/pam_google_authenticator.la pam_google_authenticator.la
    ls -lah /usr/lib64/security/ | grep google
    cp /etc/pam.d/sshd{,.bak}
    sed -i "2iauth       required     pam_google_authenticator.so nullok" /etc/pam.d/sshd
    sed -i 's|^ChallengeResponseAuthentication .*|ChallengeResponseAuthentication yes|' /etc/ssh/sshd_config
    service sshd restart
    touch /etc/security/2fa-acl.conf
    echo "+ : ALL : 192.168.1.0/24" >> /etc/security/2fa-acl.conf
    echo "+ : ALL : LOCAL" >> /etc/security/2fa-acl.conf
    echo "- : ALL : ALL" >> /etc/security/2fa-acl.conf
    sed -i "2iauth [success=1 default=ignore] pam_access.so accessfile=/etc/security/2fa-acl.conf" /etc/pam.d/sshd
    google-authenticator --time-based --disallow-reuse --window-size=17 --rate-limit=3 --rate-time=30 --force
    secretkey=$(head -n1 $HOME/.google_authenticator)
    backupcodes=$(tail -5 $HOME/.google_authenticator)
    echo
    echo "Your new secret key is: $secretkey"
    echo "Your emergency scratch codes are:"
    echo "$backupcodes"

Example Installation

    yum localinstall google-authenticator-1.03-1.el7.centos.x86_64.rpm
    Loaded plugins: fastestmirror, priorities
    Examining oogle-authenticator-1.03-1.el7.centos.x86_64.rpm: google-authenticator-1.03-1.el7.centos.x86_64
    Marking oogle-authenticator-1.03-1.el7.centos.x86_64.rpm to be installed
    Resolving Dependencies
    --> Running transaction check
    ---> Package google-authenticator.x86_64 0:1.03-1.el7.centos will be installed
    --> Finished Dependency Resolution
    
    Dependencies Resolved
    
    ==================================================================================================================================================================================================================================    ========================
    Package                                                   Arch                                        Version                                                      Repository                                                                           Size
    ==================================================================================================================================================================================================================================    ========================
    Installing:
    google-authenticator                                      x86_64                                      1.03-1.el7.centos                                        /google-authenticator-1.03-1.el7.centos.x86_    64                                       78 k
    
    Transaction Summary
    ==================================================================================================================================================================================================================================    ========================
    Install  1 Package
    
    Total size: 78 k
    Installed size: 78 k
    Is this ok [y/d/N]: y
    Downloading packages:
    Running transaction check
    Running transaction test
    Transaction test succeeded
    Running transaction
    Installing : google-authenticator-1.03-1.el7.centos.    x86_64                                                                                                                                                                                          1/1
    Verifying  : google-authenticator-1.03-1.el7.centos.    x86_64                                                                                                                                                                                          1/1
    
    Installed:
    google-authenticator.x86_64 0:1.03-1.el7.centos                                                                                                                                                                                                            
    Complete!

Installed RPM Contents

    rpm -ql google-authenticator
    /lib64/security/pam_google_authenticator.so
    /usr/bin/google-authenticator
    /usr/share/doc/google-authenticator/FILEFORMAT
    /usr/share/doc/google-authenticator/README.md
    /usr/share/doc/google-authenticator/totp.html